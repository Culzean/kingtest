﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent (typeof(CanvasGroup))]
public class Visible : MonoBehaviour
{
	protected bool visible;

	public bool activeOnStart = false;

	public void Start() {
		SetVisible(activeOnStart);
	}

    public void SetVisible(bool visible)
    {
#if DEBUG_VERBOSE
        //Debug.Log(string.Format("{0} set to Visible: {1}", this.gameObject.name, visible));
#endif
        SetVisible(visible, visible);
    }

    public void SetVisible(bool _visible, bool interactive)
	{
		CanvasGroup cg = gameObject.GetComponent<CanvasGroup>();
        cg.DOKill();
        this.visible = _visible;
        cg.interactable = (interactive) ? _visible : false;
        cg.blocksRaycasts = (interactive) ? _visible : false;
        cg.alpha = _visible ? 1.0f : 0.0f;
        //Debug.Log(string.Format("{0} set to Visible: {1} alpha: {4} interactive: {2} and blocking: {3}", this.gameObject.name, visible, cg.interactable, cg.blocksRaycasts, cg.alpha));
    }

	public void SetVisible(bool _visible, bool interactive, float alpha)
	{
		CanvasGroup cg = gameObject.GetComponent<CanvasGroup>();
		cg.DOKill();
		this.visible = _visible;
		cg.interactable = (interactive) ? _visible : false;
		cg.blocksRaycasts = (interactive) ? _visible : false;
		cg.alpha = _visible ? 1.0f : alpha;
	}
	
	public void ToggleVisible()
	{
		SetVisible(!visible);
	}

	public bool IsVisible()
	{
		return visible;
	}
}