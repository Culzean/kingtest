﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KingUnityTest
{
    public class GamePanel : Visible {

        public GameState gameState;

        protected GameController gc;

        public new void Start()
        {
            GameObject gcObject = GameObject.FindGameObjectWithTag("GameController");
            gc = gcObject.GetComponent<GameController>();
            base.Start();
        }

        public virtual void OnTransitionStart()
        {

        }
    }

}

