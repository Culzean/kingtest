﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace KingUnityTest
{
    public enum GameState
    {
        Menu = 0,
        Game = 1,
        Scores = 2,
        EndGame = 3,
    }

    public class PlayerScore
    {
        public string name;
        public int score;

        public PlayerScore(string _name, int _score)
        {
            name = _name;
            score = _score;
        }
    }

    public class GameController : MonoBehaviour {

        public GamePanel[] GamePanels = new GamePanel[4];
        public DOTweenPath timerPath;
        public ParticleSystem particleEffect;

        private GameState currentSate = GameState.Menu;
        private PlayerScore playerScore;


        // Use this for initialization
        void Start () {
		
	    }

        public void StartGame()
        {
            PlayerScore = new PlayerScore("Player1", 0);
            timerPath.DORestart();
            particleEffect.Play();
            ChangeGameState(GameState.Game);
        }

        public void ShowScoreBoard()
        {
            ChangeGameState(GameState.Scores);
        }

        public void ExitGame()
        {
#if !UNITY_EDITOR
            Application.Quit();
#else
            Debug.LogWarning("Cannot quit editor ");
#endif
        }

        public void MainMenu()
        {
            ChangeGameState(GameState.Menu);
        }

        public void EndTimer()
        {
            particleEffect.Stop();
            ChangeGameState(GameState.EndGame);
        }

        public void ChangeGameState(GameState newState)
        {
            CurrentSate = newState;
            foreach (GamePanel panel in GamePanels)
            {
                panel.SetVisible(panel.gameState == newState);
                if(panel.gameState == newState)
                {
                    panel.OnTransitionStart();
                }
            }
        }


        public PlayerScore PlayerScore
        {
            get
            {
                return playerScore;
            }

            set
            {
                playerScore = value;
            }
        }

        public GameState CurrentSate
        {
            get
            {
                return currentSate;
            }

            set
            {
                currentSate = value;
            }
        }
    }
}

