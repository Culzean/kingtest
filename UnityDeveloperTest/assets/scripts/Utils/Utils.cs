﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class Utils
{
	/// <summary>
	/// Screen the size in inches.
	/// </summary>
	/// <returns>The size in inches.</returns>
	public static float ScreenSizeInInches()
	{
		float screenWidth = Screen.width / Screen.dpi;
		float screenHeight = Screen.height / Screen.dpi;
		float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));
		return diagonalInches;
	}

	public static bool IsPointerOverGameObject( int fingerId )
	{
		EventSystem eventSystem = EventSystem.current;
		return ( eventSystem.IsPointerOverGameObject( fingerId )
			/*&& eventSystem.currentSelectedGameObject != null*/ );
	}


    /// <summary>
    /// Find which side of a line the point lies. Test is done on XZ plane
    /// </summary>
    /// <returns>+1 for point to the left, 0 for point on line, -1 for point to the right</returns>
    public static float PointLineSide( Vector3 _A, Vector3 _B, Vector3 _P )
    {
        return Mathf.Sign((_B.x - _A.x) * (_P.z - _A.z) - (_B.z - _A.z) * (_P.x - _A.x));
    }

    public static float Sigmoid(float x)
    {
        return 2 / (1 + Mathf.Exp(-2f * x)) - 1;
    }

    public static float Map( this float value, float aLow, float aHigh, float bLow, float bHigh )
    {
        float normal = Mathf.InverseLerp(aLow, aHigh, value);
        return Mathf.Lerp(bLow, bHigh, normal);
    }

    public static string GetVersion()
    {
        string build = "v";
#if UNITY_EDITOR

        build += PlayerSettings.bundleVersion;
#else
        build += Application.version;
#endif

        return build;
    }

    public static GameObject Instantiate(GameObject gameObject)
	{
		return Instantiate(gameObject, null);
	}

	public static GameObject Instantiate(GameObject gameObject, GameObject parent)
	{
		GameObject newObject = GameObject.Instantiate(gameObject);
		if(parent != null)
			newObject.transform.SetParent(parent.transform);
		newObject.transform.localPosition = Vector3.zero;
		newObject.transform.localScale = gameObject.transform.localScale;
		return newObject;
	}

	public static GameObject Instantiate2D(GameObject gameObject)
	{
		return Instantiate2D(gameObject, null);
	}

	public static GameObject Instantiate2D(GameObject gameObject, GameObject parent)
	{
		GameObject newObject = GameObject.Instantiate(gameObject);
		if(parent != null)
			newObject.transform.SetParent(parent.transform);
		RectTransform rt = newObject.GetComponent<RectTransform>();
		rt.anchoredPosition = Vector2.zero;
		rt.localScale = Vector3.one;
        rt.localRotation = Quaternion.Euler(Vector3.zero);
        rt.localPosition = Vector3.zero;
        return newObject;
	}

	public static GameObject FindParentWithTag(GameObject childObject, string tag)
	{
		Transform t = childObject.transform;
		while (t.parent != null)
		{
			if (t.parent.tag == tag)
			{
				return t.parent.gameObject;
			}
			t = t.parent.transform;
		}
		return null; // Could not find a parent with given tag.
	}

    public static void CopyLocalRotationForAllChildren( Transform a, Transform b )
    {
        CopyLocalRotation(a,b);
        if(a.childCount == 0)
        {
            return;
        }

        int index = -1;
        while(++index < a.childCount)
        {
            Transform childA = a.GetChild(index);
            Transform childB = b.GetChild(index);
            CopyLocalRotationForAllChildren(childA, childB);
        }
    }
    
    public static T GetCopyOf<T>(this Component comp, T other) where T : Component
    {
        Type type = comp.GetType();
        if (type != other.GetType()) return null; // type mis-match
        BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
        PropertyInfo[] pinfos = type.GetProperties(flags);
        foreach (var pinfo in pinfos)
        {
            if (pinfo.CanWrite)
            {
                try
                {
                    pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                }
                catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
            }
        }
        FieldInfo[] finfos = type.GetFields(flags);
        foreach (var finfo in finfos)
        {
            finfo.SetValue(comp, finfo.GetValue(other));
        }
        return comp as T;
    }

    public static void CopyLocalRotation(Transform a, Transform b)
    {
        b.localRotation = a.localRotation;
        b.localPosition = a.localPosition;
    }
    
    public static T AddComponent<T>(this GameObject go, T toAdd) where T : Component
    {
        return go.AddComponent<T>().GetCopyOf(toAdd) as T;
    }

    public static GameObject FindComponentInChildWithTag(this GameObject parent, string tag)
    {
        Transform t = parent.transform;
        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                return tr.gameObject;
            }
        }
        return null;
    }

    public static List<GameObject> FindComponentsInChildWithTag(this GameObject parent, string tag)
    {
        Transform t = parent.transform;
        List<GameObject> ret = new List<GameObject>();
        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                ret.Add(tr.gameObject);
            }
        }
        return ret;
    }

    public static class EnumUtil
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
	}

	/// <summary>
	/// Convert to enum, with default fallback. Does not use TryParse (need c#4.5) and no try catch
	/// </summary>
	/// <returns>The enum.</returns>
	/// <param name="strEnumValue">String enum value.</param>
	/// <param name="defaultValue">Default value.</param>
	/// <typeparam name="TEnum">The 1st type parameter.</typeparam>
	public static TEnum ToEnum<TEnum>(this string strEnumValue, TEnum defaultValue)
	{
		if (!Enum.IsDefined(typeof(TEnum), strEnumValue))
			return defaultValue;

		return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue);
	}

    /// <summary>
    /// Return a nicely formatted string, this is supplied by the decorator type [Description("Description for Foo")]
    /// Watch, this uses reflection so only use where the formatted is required.
    /// </summary>
    /// <param name="value"></param>
    /// <returns>description from attribute, or standard ToString() if none exists</returns>
    public static string GetDescription(this Enum value)
    {
        Type type = value.GetType();
        string name = Enum.GetName(type, value);
        if (name != null)
        {
            FieldInfo field = type.GetField(name);
            if (field != null)
            {
                System.ComponentModel.DescriptionAttribute attr =
                       Attribute.GetCustomAttribute(field,
                         typeof(System.ComponentModel.DescriptionAttribute)) as System.ComponentModel.DescriptionAttribute;
                if (attr != null)
                {
                    return attr.Description;
                }
            }
        }
        return null;
    }

    public static void FromMatrix4x4(Transform transform, Matrix4x4 matrix)
    {
        transform.localPosition = GetPosition(matrix);
        transform.localRotation = GetRotation(matrix);
        transform.localScale = GetScale(matrix);
    }
    public static Quaternion GetRotation(Matrix4x4 matrix)
    {
        var f = matrix.GetColumn(2);
        if (f == Vector4.zero)
            return Quaternion.identity;
        return Quaternion.LookRotation(f, matrix.GetColumn(1));
    }
    public static Vector3 GetPosition(Matrix4x4 matrix)
    {
        return matrix.GetColumn(3);
    }
    public static Vector3 GetScale(Matrix4x4 m)
    {
        return new Vector3(m.GetColumn(0).magnitude, m.GetColumn(1).magnitude, m.GetColumn(2).magnitude);
    }

}
