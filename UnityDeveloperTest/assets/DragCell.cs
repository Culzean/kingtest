﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace KingUnityTest
{
    public class DragCell : Visible {

        private GameGrid gameGrid;
        private CanvasGroup canvasGroup;

        void Awake()
        {
            GameObject gcObj = GameObject.FindGameObjectWithTag("GameController");
            gameGrid = gcObj.GetComponent<GameGrid>();
            Debug.Assert(gameGrid != null, "Check game scene has a grid");

            canvasGroup = GetComponent<CanvasGroup>();
        }
        // Update is called once per frame
        void Update () {

            bool dragging = GridCell.draggingCell != null;
            if (dragging)
            {
                var screenPoint = Input.mousePosition;
                screenPoint.z = 100.0f; //distance of the plane from the camera
                transform.position = Camera.main.ScreenToWorldPoint(screenPoint);
            }
            if (dragging)
            {
                UpdateGemImage(GridCell.draggingCell.GemType);
                canvasGroup.alpha = 1f;
            }
            else
            {
                canvasGroup.alpha = 0f;
            }
        }

        public void UpdateGemImage(GemTypes gemType)
        {
            Sprite sprite = gameGrid.GetGemSprite(gemType);
            GetComponent<Image>().sprite = sprite;
        }
    }

}

