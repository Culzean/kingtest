﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace KingUnityTest
{
    public class ScoreView : MonoBehaviour {

        private TextMeshProUGUI scoreText;
        private GameController gameController;

	    // Use this for initialization
	    void Start () {

            scoreText = gameObject.GetComponent<TextMeshProUGUI>();
            GameObject gcObj = GameObject.FindGameObjectWithTag("GameController");
            if(gcObj == null)
            {
                DestroyImmediate(this.gameObject);
            }
            gameController = gcObj.GetComponent<GameController>();
            if (gameController == null)
            {
                DestroyImmediate(this.gameObject);
            }
        }
	
	    // Update is called once per frame
	    void Update () {

            if (gameController.CurrentSate != GameState.Game) return;

            var score = gameController.PlayerScore.score;
            scoreText.text = "Score : " + score;

        }
    }

}

