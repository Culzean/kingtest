﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;

namespace KingUnityTest
{

    public enum GemTypes
    {
        Blue = 0,
        Green = 1,
        Purple = 2,
        Red = 3,
        Yellow = 4,
        Floor = 5,

        Count = 6,

    }

    public class GameGrid : MonoBehaviour {

        private class SwapOrder
        {
            public readonly Vector3Int cellA;
            public readonly Vector3Int cellB;

            public SwapOrder(Vector3Int _cellA, Vector3Int _cellB)
            {
                cellA = _cellA;
                cellB = _cellB;
            }
        }

        public Transform gridRoot;
        public GameObject cellPrefab;
        public GameObject cellFloorPrefab;
        public Visible cellHighlighter;

        public Sprite[] gemSprites = new Sprite[(int)GemTypes.Count];


        private List<GridCell> gridCells;
        private GameScoring gameScoring;
        private GameController gameController;
        private Grid grid;
        private bool gridActive = false;
        private bool matchingCellsCheckRequired = false;
        private SwapOrder previousSwap;

        public const int GRID_WIDTH = 8;
        public const int GRID_HEIGHT = 10;


        private void Awake()
        {
            if(gridRoot == null)
            {
                Debug.LogError("No grid root has been assigned. Please ensure the gridRoot game tag exists in the scene");
            }
            Grid = gridRoot.gameObject.GetComponent<Grid>();
            gameScoring = gameObject.GetComponent<GameScoring>();
            gameController = gameObject.GetComponent<GameController>();
        }

        // Use this for initialization
        void Start () {
            BuildGrid();

        }
	
        private void BuildGrid()
        {
            GridActive = true;

            gridCells = new List<GridCell>(GRID_HEIGHT * GRID_WIDTH);

            for (int y=0; y<GRID_HEIGHT; y++)
            {
                for (int x = 0; x < GRID_WIDTH; x++)
                {
                    bool isFloor = y == 0;

                    GemTypes gemType = (GemTypes)UnityEngine.Random.Range(0, (int)GemTypes.Count-1);
                    gemType = isFloor ? GemTypes.Floor : gemType;
                    Vector3Int coord = new Vector3Int(x,y,0);
                    var script = CreateCell(coord, gemType);

                    gridCells.Add(script);
                }
            }

            var connected = FindMatchingCells();
            foreach(Vector3Int coord in connected)
            {
                GridCell cell = GetCell(coord);
                Destroy(cell.gameObject);
                //cell.gameObject.GetComponent<CanvasGroup>().alpha = 0.5f;
            }

            GridActive = false;
        }

	    void Update () {
		
            if(!GridActive && matchingCellsCheckRequired && BoardIsStationary())
            {
                bool foundMatching = CheckForMatchingCells();
                if(previousSwap != null && !foundMatching)
                {
                    StartCoroutine(InvalidSwapAndReturn());
                }
                else
                {
                    previousSwap = null;
                }

            }
            //ensure cells are replemished
            for(int x =0; x<GRID_WIDTH; x++)
            {
                var cell = GetCell(x, GRID_HEIGHT - 1);
                if(cell == null)
                {
                    GemTypes gemType = (GemTypes)UnityEngine.Random.Range(0, (int)GemTypes.Count - 1);
                    Vector3Int coord = new Vector3Int(x, GRID_HEIGHT - 1, 0);
                    var newScript = CreateCell(coord, gemType);
                    UpdateCellPosition(newScript, Vector3Int.zero);
                }
            }
	    }

        public IEnumerator InvalidSwapAndReturn()
        {
            if (previousSwap == null) yield break;

            GridCell cellA = GetCell(previousSwap.cellA);
            GridCell cellB = GetCell(previousSwap.cellB);

            previousSwap = null;
            if (cellA == null || cellB == null) yield break;

            yield return StartCoroutine(SwapCell(cellA, cellB, false));

            yield return null;
        }

        public IEnumerator MoveCellToCoord(GridCell cellA, Vector3Int coord)
        {
            GridActive = true;

            Vector3 cellTargetPosition = Grid.GetCellCenterWorld(cellA.coord);
            yield return cellA.transform.DOMove(cellTargetPosition, 0.33f)
                .SetEase(Ease.InBounce)
                .WaitForCompletion();

            GridActive = false;
        }

        public IEnumerator SwapCell(GridCell cellA, GridCell cellB, bool newOrder = true)
        {
            GridActive = true;
            if(newOrder)
            {
                previousSwap = new SwapOrder(cellA.coord, cellB.coord);
            }

            cellA.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            cellB.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;

            Vector3 cellAStartPosition = Grid.GetCellCenterWorld(cellA.coord);
            Vector3 cellBTargetPosition = Grid.GetCellCenterWorld(cellB.coord);
            cellA.transform.DOMove(cellBTargetPosition, 0.33f)
                .SetEase(Ease.OutBounce);

            yield return new WaitForSeconds(0.22f);

            yield return cellB.transform.DOMove(cellAStartPosition, 0.33f)
                .SetEase(Ease.InBounce)
                .WaitForCompletion();

            cellA.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            cellB.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            GridActive = false;
        }
        
        public bool ValidSwap(GridCell a, GridCell b)
        {
            return Vector3Int.Distance(a.coord, b.coord) <= 1f;
        }

        public bool BoardIsStationary()
        {
            for(int i=0; i<gridCells.Count; i++)
            {
                GridCell cell = gridCells[i];
                if (cell != null && !cell.HasStopped())
                {
                    return false;
                }
            }

            return true;
        }

        public List<Vector3Int> FindMatchingCells()
        {
            List<Vector3Int> ret = new List<Vector3Int>();
            Stack<Vector3Int> tempDuplicates = new Stack<Vector3Int>();

            for(int row=1; row<GRID_WIDTH; row++)
            {
                GridCell prevCell = GetCell(0, row);

                for (int col=1; col<GRID_HEIGHT-1; col++)
                {
                    GridCell currentCell = GetCell(col, row);
                    if(currentCell == null || prevCell == null)
                    {
                        continue;
                    }
                    if (prevCell.GemType == currentCell.GemType)
                    {
                        if (tempDuplicates.Count == 0)
                        {
                            tempDuplicates.Push(prevCell.coord);
                        }
                        tempDuplicates.Push(currentCell.coord);
                    }
                    else if(tempDuplicates.Count >= 3)
                    {
                        ret.AddRange(tempDuplicates);
                        tempDuplicates.Clear();
                    }
                    else
                    {
                        tempDuplicates.Clear();
                    }
                    prevCell = currentCell;
                }

                if (tempDuplicates.Count >= 3)
                {
                    ret.AddRange(tempDuplicates);
                }
                tempDuplicates.Clear();
            }

            for (int col = 0; col < GRID_HEIGHT-1; col++)
            {
                GridCell prevCell = GetCell(col, 0);

                for (int row = 1; row < GRID_WIDTH; row++)
                {
                    GridCell currentCell = GetCell(col, row);
                    if (currentCell == null || prevCell == null)
                    {
                        continue;
                    }
                    if (prevCell.GemType == currentCell.GemType)
                    {
                        if(tempDuplicates.Count == 0)
                        {
                            tempDuplicates.Push(prevCell.coord);
                        }
                        tempDuplicates.Push(currentCell.coord);
                    }
                    else if (tempDuplicates.Count >= 3)
                    {
                        ret.AddRange(tempDuplicates);
                        tempDuplicates.Clear();
                    }
                    else
                    {
                        tempDuplicates.Clear();
                    }
                    prevCell = currentCell;
                }

                if (tempDuplicates.Count >= 3)
                {
                    ret.AddRange(tempDuplicates);
                }
                tempDuplicates.Clear();
            }

            return ret.Distinct().ToList();
        }

        public void UpdateCellPosition(GridCell cell, Vector3Int oldCoord)
        {
            Vector3Int newCoord = grid.WorldToCell(cell.transform.position);
            int newIndex = GridPositionToListIndex(newCoord);

            if (newCoord == oldCoord || newIndex < 0 || newIndex >= gridCells.Count) return;

            int oldIndex = GridPositionToListIndex(oldCoord);
            if(oldIndex > 0 && gridCells[oldIndex] == cell)
            {
                gridCells[oldIndex] = null;
            }
            gridCells[newIndex] = cell;
            cell.coord = newCoord;

            matchingCellsCheckRequired = gameController.CurrentSate == GameState.Game;
        }

        private bool CheckForMatchingCells()
        {
            var matching = FindMatchingCells();

            gameScoring.Scoring(matching);

            bool matchingCellsExist = matching.Count > 0;

            foreach (Vector3Int coord in matching)
            {
                GridCell cell = GetCell(coord);
                Destroy(cell.gameObject);
            }

            matchingCellsCheckRequired = false;

            return matchingCellsExist;
        }

        public Sprite GetGemSprite(GemTypes type)
        {
            int typeIndex = (int)type;
            Debug.Assert(typeIndex >= 0 && typeIndex < gemSprites.Length, "Gem type provided is out of bounds for the number of sprites.");
            return gemSprites[typeIndex];
        }

        private GridCell CreateCell(Vector3Int coord, GemTypes gemType)
        {
            bool isFloor = gemType == GemTypes.Floor;
            GameObject newCell = Utils.Instantiate2D(isFloor ? cellFloorPrefab : cellPrefab, gridRoot.gameObject);

            newCell.transform.position = Grid.GetCellCenterWorld(coord);

            Sprite sprite = GetGemSprite(gemType);

            GridCell cellScript = newCell.GetComponent<GridCell>();
            cellScript.Init(coord, sprite, gemType);

            return cellScript;
        }

        private GridCell GetCell(Vector3Int coord)
        {
            return GetCell(coord.x, coord.y);
        }

        private GridCell GetCell(int x, int y)
        {
            int index = GridPositionToListIndex(new Vector3Int(x, y, 0));
            return index >= 0 ? gridCells[index] : null;
        }

        private int GridPositionToListIndex(Vector3Int coord)
        {
            if (coord.x < 0 || coord.x > GRID_WIDTH) return -1;
            if (coord.y < 0 || coord.y > GRID_HEIGHT) return -1;
            
            return (coord.y * GRID_WIDTH) + coord.x;
        }

        public bool GridActive
        {
            get
            {
                return gridActive;
            }

            set
            {
                gridActive = value;
            }
        }

        public Grid Grid
        {
            get
            {
                return grid;
            }

            set
            {
                grid = value;
            }
        }
    }

}
