﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace KingUnityTest
{


    public class GridCell : MonoBehaviour, IDropHandler, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IDragHandler {

        private static GridCell selectedCell = null;
        public static GridCell draggingCell = null;

        private GemTypes gemType;
        private CellHighlighter cellHighlighter;
        private Rigidbody2D body;
        private CanvasGroup canvasGroup;

        public Vector3Int coord;

        private GameGrid gameGrid;

        // Use this for initialization
        void Awake () {

            GameObject boardRootObj = GameObject.FindGameObjectWithTag("GameController");
            Debug.Assert(boardRootObj != null, "Cannot find GameController, please ensure an object with this tag exists in the game scene");
            gameGrid = boardRootObj.GetComponent<GameGrid>();
            Debug.Assert(gameGrid != null, "Cannot GameGrid component, please ensure GameController has this component attached.");

            cellHighlighter = GameObject.FindObjectOfType<CellHighlighter>();
            Debug.Assert(cellHighlighter != null, "Cell highlighter object is required in game scene");

            body = gameObject.GetComponent<Rigidbody2D>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Init(Vector3Int _coord, Sprite _sprite, GemTypes _gemType)
        {
            gemType = _gemType;
            GetComponent<Image>().sprite = _sprite;
            coord = _coord;
        }

        // Update is called once per frame
        void Update () {
		
            var coordUpdated = gameGrid.Grid.WorldToCell(this.transform.position);
            if (!gameGrid.GridActive &&
                gemType != GemTypes.Floor &&
                coordUpdated != coord)
            {
                gameGrid.UpdateCellPosition(this, coord);
            }
	    }

        public void OnDrag(PointerEventData eventData)
        {

        }

        public void OnEndDrag(PointerEventData eventData)
        {
            canvasGroup.alpha = 1f;
            if (!gameGrid.GridActive)
            {
                StartCoroutine(gameGrid.MoveCellToCoord(draggingCell, draggingCell.coord));
            }
            draggingCell = null;
            selectedCell = null;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if(gameGrid.ValidSwap(draggingCell, this) && draggingCell != this)
            {
                StartCoroutine(gameGrid.SwapCell(draggingCell, this));
            }
            else
            {
                StartCoroutine(gameGrid.MoveCellToCoord(draggingCell, draggingCell.coord));
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            draggingCell = this;
            canvasGroup.alpha = 0f;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (gameGrid.GridActive) return;
            
            if (SelectedCell == null)
            {
                SelectedCell = this;
                cellHighlighter.ShowHighlight(this.coord);
            }
            else
            {
                if(gameGrid.ValidSwap(SelectedCell, this))
                {
                    StartCoroutine(gameGrid.SwapCell(SelectedCell, this));
                }
                SelectedCell = null;
                cellHighlighter.HideHighlight();
            }
        }

        public bool HasStopped()
        {
            return body.velocity.magnitude < 0.33f;
        }

        public IEnumerable GetNeighbours()
        {
            if(coord.x > 0) yield return new Vector3Int(coord.x-1, coord.y, 0);
            if (coord.y < GameGrid.GRID_HEIGHT-1) yield return new Vector3Int(coord.x, coord.y + 1, 0);
            if (coord.x < GameGrid.GRID_WIDTH-1) yield return new Vector3Int(coord.x + 1, coord.y, 0);
            if (coord.y > 0) yield return new Vector3Int(coord.x, coord.y - 1, 0);
        }

        public IEnumerable GetVerticalNeighbours()
        {
            if (coord.y < GameGrid.GRID_HEIGHT - 1) yield return new Vector3Int(coord.x, coord.y + 1, 0);
            if (coord.y > 0) yield return new Vector3Int(coord.x, coord.y - 1, 0);
        }

        public IEnumerable GetHorizontalNeighbours()
        {
            if (coord.x > 0) yield return new Vector3Int(coord.x - 1, coord.y, 0);
            if (coord.x < GameGrid.GRID_WIDTH - 1) yield return new Vector3Int(coord.x + 1, coord.y, 0);
        }

        public GemTypes GemType
        {
            get
            {
                return gemType;
            }

            set
            {
                gemType = value;
            }
        }

        public static GridCell SelectedCell
        {
            get
            {
                return selectedCell;
            }

            set
            {
                selectedCell = value;
            }
        }
        
    }
}
