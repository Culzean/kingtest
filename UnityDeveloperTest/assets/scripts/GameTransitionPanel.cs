﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace KingUnityTest
{
    public class GameTransitionPanel : GamePanel {

        public TextMeshProUGUI scoreText;

        public override void OnTransitionStart()
        {
            base.OnTransitionStart();
            StartCoroutine(ShowScore());
        }

        public IEnumerator ShowScore ()
        {
            string score = gc.PlayerScore.score.ToString();
            yield return scoreText.DOText(score, 0.99f, true, ScrambleMode.Numerals).WaitForCompletion();

            yield return new WaitForSeconds(3.4f);
            gc.ChangeGameState(GameState.Scores);
        }
    }

}

