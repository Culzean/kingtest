﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KingUnityTest
{
    public class GameScoring : MonoBehaviour {

        private GameController gameController;


	    // Use this for initialization
	    void Start () {

            gameController = gameObject.GetComponent<GameController>();
        }
	
	    public void Scoring(List<Vector3Int> scoringCells)
        {
            gameController.PlayerScore.score += 100;
        }
    }

}

