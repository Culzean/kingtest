﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KingUnityTest
{

    public class CellHighlighter : Visible {

        private GameGrid gameGrid;

        public new void Start()
        {
            GameObject gcObj = GameObject.FindGameObjectWithTag("GameController");
            gameGrid = gcObj.GetComponent<GameGrid>();
            Debug.Assert(gameGrid != null, "Check game scene has a grid");

            base.Start();
        }

        public void ShowHighlight(Vector3Int location)
        {
            transform.position = gameGrid.Grid.GetCellCenterWorld(location);
            SetVisible(true);
        }

        public void HideHighlight()
        {
            SetVisible(false);
        }
    }

}
